# Attrap

Un logiciel qui récupère les derniers recueils des actes administratifs (RAA) pour y rechercher certains mots-clés prédéfinis.

Conçu pour être utilisé dans une CI.

Peut envoyer par email et sur Mastodon  les résultats, par exemple avec <a rel="me" href="https://mamot.fr/@AttrapSurveillance">@AttrapSurveillance\@mamot.fr</a>.

## Installation

Il est recommandé d'utiliser virtualenv :

```bash
virtualenv --python=/usr/bin/python3 .
source bin/activate
pip3 install -r requirements.txt
```

Vous devez avoir installé les données `eng` et `fra` de Tesseract et le démon Tor.

## Utilisation

Pour lancer la récupération de toutes les administrations supportées :

```bash
make
```

Attention, le premier lancement prendra plusieurs jours ! Si vous utilisez une CI, vous devez mettre en cache le dossier `data/` afin que les fichiers déjà analysés ne soient pas téléchargés à chaque lancement.

Il est possible de ne lancer l'analyse que pour une seule administration, avec la commande : `./cli.py identifiant`

## Options

Les options suivantes peuvent être précisées, par un paramètre si l'utilitaire `cli.py` est utilisé, ou par une variable d'environnement :

| CLI | Variable d'environnement | Signification | Valeur par défaut |
|---|---|---|---|
| `--keywords`, `-k` | `KEYWORDS` | Liste des mots-clés recherchés, séparés par une virgule. | Aucune |
| `--not-before` | `NOT_BEFORE` | Date (format relatif `1 week` ou `YYYY-MM-DD`) avant laquelle les RAA ne sont pas analysés. | `2024-01-01` |
| `--smtp-hostname` | `SMTP_HOSTNAME` | Nom d'hôte SMTP. | `localhost` |
| `--smtp-username` | `SMTP_USERNAME` | Nom d'utilisateur SMTP. | Aucun |
| `--smtp-password` | `SMTP_PASSWORD` | Mot de passe SMTP. | Aucun |
| `--smtp-port` | `SMTP_PORT` | Port SMTP. | `587` |
| `--smtp-starttls` | `SMTP_STARTTLS` | Si spécifié, la connexion SMTP se fait avec STARTTLS. | Non-spécifié |
| `--smtp-ssl` | `SMTP_SSL` | Si spécifié, la connexion SMTP se fait avec SSL. | Non-spécifié |
| `--email-from`, `-f` | `EMAIL_FROM` | Adresse de courrier électronique expéditrice des notifications. | Aucune (désactive l'envoi) |
| `--email-to`, `-t` | `EMAIL_TO` | Adresses de courriers électroniques destinataires des notifications, séparées par une virgule. | Aucune (désactive l'envoi) |
| `--**-email-to` | `--**-EMAIL-TO` | Pour chaque administration dont l'identifiant est **, adresses de courriers électroniques destinataires des notifications, séparées par une virgule, uniquement si l'analyse concerne cette administration en particulier. La liste s'ajoute à celle précisée dans `--email-to`. | Aucune |
| `--mastodon-access-token` | `MASTODON_ACCESS_TOKEN` | Jeton d'accès pour publier sur Mastodon. | Aucun (désactive la publication sur Mastodon) |
| `--mastodon-instance` | `MASTODON_INSTANCE` | URL de l'instance Mastodon de publication (doit inclure "http://" ou "https://"). | Aucune (désactive la publication sur Mastodon) |
| `-v` | `VERBOSE` | Si spécifié, relève le niveau de verbosité à INFO. | Non-spécifié |
| `-vv` | `VVERBOSE` | Si spécifié, relève le niveau de verbosité à DEBUG. | Non-spécifié |

Vous pouvez également activer le safe mode en spécifiant la variable d'environnement `SAFE_MODE`. Cela désactive Tor et limite les requêtes à une toutes les 30 secondes. Cette option ne doit être utilisée qu'en cas de blocage généralisé de Tor. Attention : avec le safe mode, la CI se connecte directement au serveur, et son IP risque d'être bloquée si trop de requêtes sont lancées. Pensez donc à limiter le nombre de jobs qui s'exécutent en parallèle si vous devez activer ce mode. Les requêtes via Selenium ne sont pas impactées par le safe mode.

## Administrations supportées

- Préfecture de police de Paris (identifiant : `ppparis`)
- Préfecture de Haute-Corse (identifiant : `pref2b`)
- Préfecture de l'Allier (identifiant : `pref03`)
- Préfecture des Alpes-de-Haute-Provence (identifiant : `pref04`)
- Préfecture des Hautes-Alpes (identifiant : `pref05`)
- Préfecture des Alpes-Maritimes (identifiant : `pref06`)
- Préfecture de l'Ariège (identifiant : `pref09`)
- Préfecture de l'Aube (identifiant : `pref10`)
- Préfecture des Bouches-du-Rhône (identifiant : `pref13`)
- Préfecture du Doubs (identifiant : `pref25`)
- Préfecture de la Haute-Garonne (identifiant : `pref31`)
- Préfecture de la Gironde (identifiant : `pref33`)
- Préfecture de l'Hérault (identifiant : `pref34`)
- Préfecture d'Ille-et-Vilaine (identifiant : `pref35`)
- Préfecture de l'Isère (identifiant : `pref38`)
- Préfecture du Jura (identifiant : `pref39`)
- Préfecture de la Loire (identifiant : `pref42`)
- Préfecture de la Loire-Atlantique (identifiant : `pref44`)
- Préfecture du Nord (identifiant : `pref59`)
- Préfecture du Pas-de-Calais (identifiant : `pref62`)
- Préfecture des Pyrénées-Atlantiques (identifiant : `pref64`)
- Préfecture des Hautes-Pyrénées (identifiant : `pref65`)
- Préfecture des Pyrénées-Orientales (identifiant : `pref66`)
- Préfecture du Rhône (identifiant : `pref69`)
- Préfecture de la Savoie (identifiant : `pref73`)
- Préfecture de Paris (identifiant : `pref75`)
- Préfecture de la Somme (identifiant : `pref80`)
- Préfecture du Tarn (identifiant : `pref81`)
- Préfecture du Var (identifiant : `pref83`)
- Préfecture de la Haute-Vienne (identifiant : `pref87`)
- Préfecture des Hauts-de-Seine (identifiant : `pref92`)
- Préfecture de Seine-Saint-Denis (identifiant : `pref93`)
- Préfecture du Val-de-Marne (identifiant : `pref94`)
- Préfecture de Mayotte (identifiant : `pref976`)
- Préfecture d'Île-de-France (identifiant : `prefidf`)
- Préfecture de Provence-Alpes-Côte-d'Azur (identifiant : `prefpaca`)

## Contributions

Les contributions à ce projet sont les bienvenues !

Chaque administration est gérée par un fichier dont le nom correspond à son identifiant (`Attrap_XXX.py`). Commencez par copier un de ces fichiers puis adaptez son code à l'administration que vous voulez ajouter. Il est impératif de lancer le moins de requêtes possibles vers le site de l'administration : lorsqu'une administration a une page par année ou par mois, ne lancez une requête que vers les pages qui correspondent à la plage temporelle demandée dans la valeur de configuration `NOT_BEFORE`.

Vous pouvez lancer la commande suivante pour connaître fonctions disponibles pour récupérer les RAA sur le site d'une administration :

```
bin/python -m pydoc Attrap
```

Avant d'ouvrir une merge request, assurez-vous que :
- l'administration est activée dans `cli.py` et dans `Makefile` ;
- il existe un job dans la CI (`.gitlab-ci.yml`) pour l'administration ;
- le fichier de README indique que la nouvelle administration est supportée ;
- vous n'avez qu'un seul commit par nouvelle préfecture (sinon, il faut faire un squash), de la forme `identifiant_de_l'administration: ajout de nom_complet_de_l'administration` ;
- `make lint` ne renvoie pas d'erreur.

Vous pouvez rejoindre le salon de discussion Matrix du projet : `#Attrap:laquadrature.net`.

## Licence

[CeCILL_V2.1-fr](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (voir le fichier `LICENSE`)
