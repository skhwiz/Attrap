import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_prefpaca(Attrap):

    # Config
    __HOST = 'https://www.prefectures-regions.gouv.fr'
    __RAA_PAGE = f'{__HOST}/provence-alpes-cote-dazur/Documents-publications'
    __USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture de Provence-Alpes-Côte-d\'Azur'
    short_code = 'prefpaca'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.enable_tor(10)

    def get_raa(self, keywords):
        # On récupère les pages d'années
        year_pages = []
        for year_page in self.get_sub_pages_with_pager(
            self.__RAA_PAGE,
            'article.news-list-item header h2.news-list-title a',
            'article.article div.content-pagination ul.pagination li.next a',
            None,
            self.__HOST
        ):
            year = Attrap.guess_date(year_page['name'].strip(), 'RAA ([0-9]{4})').year
            if year < 9999 and year >= self.not_before.year:
                year_pages.append(year_page['url'])

        elements = []
        for year_page in year_pages:
            page_content = self.get_page(year_page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # Pour chaque balise a, on regarde si c'est un PDF, et si oui on le parse
        for a in soup.select('main div.container.main-container div.col-main article.article div.texte div a.link-download'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']
                url = unquote(url)
                name = a.find('span').get_text().strip()
                # On devine la date du RAA à partir du nom de fichier
                guessed = Attrap.guess_date(name, '((?:[0-9]{2}|[0-9]{1})(?:er){0,1}[ _](?:[a-zéû]{3,9})[ _](?:[0-9]{4}|[0-9]{2}))')
                if (guessed == datetime.datetime(9999, 1, 1, 0, 0)):
                    date = None
                else:
                    date = guessed

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
