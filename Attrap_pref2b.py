import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref2b(Attrap):

    # Config
    __HOST = 'https://www.haute-corse.gouv.fr'
    __RAA_PAGE = f'{__HOST}/Publications/Publications-administratives-et-legales/Recueils-des-actes-administratifs'
    __USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture de Haute-Corse'
    short_code = 'pref2b'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        # La préfecture de Haute-Corse organise son site avec une page dédiée à l'année N, une autre dédiée à l'année N-1,
        # puis les années d'avant sont regroupées ensemble sur deux pages. On doit donc parser les pages jusqu'à ce qu'on ne
        # tombe plus sur des cartes d'années.

        pages_to_parse = []

        page_content = self.get_page(self.__RAA_PAGE, 'get').content
        for card in self.get_sub_pages(
            page_content,
            'div.fr-card__body div.fr-card__content h2.fr-card__title a',
            self.__HOST,
            False
        ):
            if Attrap.guess_date(card['name'], '[a-z]* ([0-9]{4})').year >= self.not_before.year:
                pages_to_parse.append(card['url'])
            else:
                # Si on n'a pas trouvé une page d'année, on tente de parser la page à la rechercge
                # de sous-pages (et sinon on ignore la page)
                page_content = self.get_page(card['url'], 'get').content
                for card in self.get_sub_pages(
                    page_content,
                    'div.fr-card__body div.fr-card__content h2.fr-card__title a',
                    self.__HOST,
                    False
                ):
                    if Attrap.guess_date(card['name'], '[a-z]* ([0-9]{4})').year >= self.not_before.year:
                        pages_to_parse.append(card['url'])

        # Pour chaque année, on cherche les sous-pages de mois
        month_pages_to_parse = []
        for year_page in pages_to_parse:
            for month_page in self.get_sub_pages_with_pager(
                year_page,
                'div.fr-card__body div.fr-card__content h2.fr-card__title a.fr-card__link',
                'ul.fr-pagination__list li a.fr-pagination__link.fr-pagination__link--next',
                None,
                self.__HOST
            ):
                if Attrap.guess_date(month_page['name'], '([a-zûé]* [0-9]{4})').replace(day=1) >= self.not_before.replace(day=1):
                    month_pages_to_parse.append(month_page['url'])

        # On parse les pages contenant des RAA
        elements = []
        for page in month_pages_to_parse:
            page_content = self.get_page(page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        self.parse_raa(elements[::-1], keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère chaque balise a
        for a in soup.select('div.fr-downloads-group.fr-downloads-group--bordered ul li a'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(a.find('span').get_text().split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
