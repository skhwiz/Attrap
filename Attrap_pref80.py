import os
import datetime
import logging

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap

logger = logging.getLogger(__name__)


class Attrap_pref80(Attrap):

    # Config
    __HOST = 'https://www.somme.gouv.fr'
    __RAA_PAGE = {
        '2024': f'{__HOST}/Publications/Recueil-des-actes-administratifs-du-departement-de-la-Somme/Annee-2024',
        '2023': f'{__HOST}/Publications/Recueil-des-actes-administratifs-du-departement-de-la-Somme/Annee-2023',
        '2022': f'{__HOST}/Publications/Recueil-des-actes-administratifs-du-departement-de-la-Somme/Annee-2022',
        '2021': f'{__HOST}/Publications/Recueil-des-actes-administratifs-du-departement-de-la-Somme/Annee-2021',
        '2020': f'{__HOST}/Publications/Recueil-des-actes-administratifs-du-departement-de-la-Somme/Annee-2020',
        '2019': f'{__HOST}/Publications/Recueil-des-actes-administratifs-du-departement-de-la-Somme/Annee-2019'
    }
    __USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture de la Somme'
    short_code = 'pref80'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        year_pages_to_parse = []
        if self.not_before.year <= 2024:
            year_pages_to_parse.append(self.__RAA_PAGE['2024'])
        if self.not_before.year <= 2023:
            year_pages_to_parse.append(self.__RAA_PAGE['2023'])
        if self.not_before.year <= 2022:
            year_pages_to_parse.append(self.__RAA_PAGE['2022'])
        if self.not_before.year <= 2021:
            year_pages_to_parse.append(self.__RAA_PAGE['2021'])
        if self.not_before.year <= 2020:
            year_pages_to_parse.append(self.__RAA_PAGE['2020'])
        if self.not_before.year <= 2019:
            year_pages_to_parse.append(self.__RAA_PAGE['2019'])

        # Pour chaque page Année, on récupère la liste des RAA
        elements = []
        for year_page in year_pages_to_parse:
            page_content = self.get_page(year_page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # Pour chaque balise a, on regarde si c'est un PDF, et si oui on le
        # parse
        for a in soup.select('div.fr-text--lead.fr-my-3w p a.fr-link'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                # On enlève les espaces insécables, les double-espaces, et le texte « Télécharger » de certains liens
                name = a.get_text().replace('Télécharger ', '').strip().replace(u"\u00A0", ' ').replace('  ', ' ')
                if name and not name == '':
                    # Certains RAA de la Somme ont une ligne avec les détails du fichier. Si cette ligne
                    # est disponible, on la parse, sinon on devine la date à partir du nom
                    date = None
                    if a.find('span'):
                        date = datetime.datetime.strptime(a.find('span').get_text().split(' - ')[-1].strip(), '%d/%m/%Y')
                    else:
                        regex = '.* n°.*(?:du)? ([0-9]*(?:er)? [a-zéû]* (?:[0-9]{4}|[0-9]{2}))'
                        date = Attrap.guess_date(name, regex)
                        # Parfois, il manque l'année dans le nom du RAA, alors on essaie avec l'année de la page
                        if date.year == 9999:
                            page_year = soup.select('nav.fr-breadcrumb div.fr-collapse ol.fr-breadcrumb__list li a.fr-breadcrumb__link.breadcrumb-item-link')[-1].get_text().replace('Année ', '').strip()
                            date = Attrap.guess_date(f'{name} {page_year}', regex)

                            # Parfois, c'est que le fichier n'est pas un RAA mais un arrêté seul
                            if date.year == 9999:
                                date = Attrap.guess_date(name, '([0-9]*(?:er)? [a-zéû]* [0-9]{4})')

                    if date.year == 9999:
                        logger.warning(f'On ignore {name} (URL : {url})')
                    else:
                        raa = Attrap.RAA(url, date, name)
                        elements.append(raa)
        return elements[::-1]
